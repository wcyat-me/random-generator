declare module "wcyat-rg" {
    export function generate(
    options:{
        include:{
            numbers:boolean,
            upper:boolean,
            lower:boolean,
            special:boolean
        },
        digits: number
    }): string }